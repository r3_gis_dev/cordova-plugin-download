package com.phonegap.plugin.DownloadPlugin;

import java.io.File;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import android.util.Log;

public class DatabaseDownloadListener implements DataDownloadListener {
	
	private File dbFile;
	
	public DatabaseDownloadListener(File dbFile) throws FileNotFoundException  {
		if (!dbFile.exists())
    	{ 
    		throw new FileNotFoundException("Database "+dbFile.getAbsolutePath()+" does not exists!");
    	}
		
		this.dbFile = dbFile;
	}
	
	public String getListenerName() {
		return "database";
	}
	
	public void onDownloadStarted() {
		
	}
	
	public void onDownloadFinished(File file) throws FileNotFoundException, IOException {
		InputStream in = new FileInputStream(file);
		Log.d("DatabaseDownloadListener", "database origin " + file);
		Log.d("DatabaseDownloadListener", "database destination " + this.dbFile);
		OutputStream out = new FileOutputStream(this.dbFile);
		
    	// Transfer bytes from in to out
    	byte[] buf = new byte[1024];
    	int len;
    	while ((len = in.read(buf)) > 0) {
    		out.write(buf, 0, len);
    	}
    	out.flush();
    	out.close();
    	in.close();
    	
    	// Delete downloaded file
    	file.delete();
	}
}
