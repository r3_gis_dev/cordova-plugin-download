package com.phonegap.plugin.DownloadPlugin;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public interface DataDownloadListener {
	String getListenerName();
	void onDownloadStarted();
	void onDownloadFinished(File file) throws FileNotFoundException, IOException;
}
