package com.phonegap.plugin.DownloadPlugin;

import java.util.ArrayList;

public class DataDownloadEvent {
	static ArrayList<DataDownloadListener> listeners = new ArrayList<DataDownloadListener>();
	
    public static void setOnDownload(DataDownloadListener listener){
    	DataDownloadEvent.listeners.add(listener);
    }
}
