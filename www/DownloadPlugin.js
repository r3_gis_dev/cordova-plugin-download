cordova.define("cordova/plugin/DownloadPlugin", function (require, exports, module) {
    var exec = require("cordova/exec");
    if (window.plugins && window.plugins.cordovaForWeb) {
        exec = require("cordova/plugins/cordovaForWeb");
    }
    
    
    module.exports = {
        get: function (message, win, fail) {
            exec(win, fail, "Downloader", "get", [message]);
        }
    };
});
//-------------------------------------------------------------------
if(!window.plugins) {
    window.plugins = {};
}
if (!window.plugins.downloader) {
    window.plugins.downloader = cordova.require("cordova/plugin/DownloadPlugin");
}